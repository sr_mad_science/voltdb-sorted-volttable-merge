
/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with VoltDB.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.voltdb.VoltTable;
import org.voltdb.client.Client;
import org.voltdb.client.ClientConfig;
import org.voltdb.client.ClientFactory;
import org.voltdb.client.ClientResponse;
import org.voltdb.client.ClientResponseWithPartitionKey;
import org.voltdb.client.NoConnectionsException;
import org.voltdb.client.ProcCallException;
import org.voltdb.sortedvolttable.VoltDBTableSortedMergeWrangler;

import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class TestSortedMergeWrangler {

    private static final String VARCHAR_100 = "VARCHAR(100)";
    private static final String BIGINT = "BIGINT";
    

  

    public static void main(String[] args) {

        TestSortedMergeWrangler tsmw = new TestSortedMergeWrangler();
        tsmw.runTest();
    }

   

    /**
     * 
     */
    @Test
    public  void runTest() {
        try {
            Random r = new Random(42);
            int rowCount = 10000;
            int bytePayloadLength = 10;
            ComplainOnErrorCallback coec = new ComplainOnErrorCallback();

            String jarName = "/Users/drolfe/Desktop/EclipseWorkspace/voltdb-sorted-volttable-merge/jars/sort_test.jar";

            String[] datatypes = { VARCHAR_100, BIGINT };
            String[] datatypeClassname = { "Varchar","Bigint"};
   
            Client c = connectVoltDB("localhost");

            for (int d = 0; d < datatypes.length; d++) {
                dropTables(c,jarName);

                createTables(bytePayloadLength, jarName, c, datatypes[d],datatypeClassname[d]);

                msg("Create test data for " + datatypes[d]);
                for (int i = 0; i < rowCount; i++) {
                    c.callProcedure(coec, "sort_test.INSERT", "PK1_" + (i % 10), getPk2(i, datatypes[d]),
                            makeBinaryPayload(r, bytePayloadLength));
                }
                c.drain();
                msg("Done creating test data for " + datatypes[d]);

                msg("Run test on " + datatypes[d]);
                for (int i = 0; i < rowCount; i++) {
                    testValues(c, "PK1_" + (i % 10), getPk2(i, datatypes[d]), r.nextInt(1000),datatypeClassname[d]);

                }
                c.drain();
                msg("Done.");

                dropTables(c,jarName);

            }

            c.close();

            System.exit(0);

        } catch (Exception e) {
            msg(e.getMessage());
        }
    }

    private static Object getPk2(int i, String type) {

        Object retval = null;

        if (type.equals(VARCHAR_100)) {
            retval = "PK2_" + i;
        } else if (type.equals(BIGINT)) {
            retval = new Long(i);
        } 

        return retval;
    }

    /**
     * @param bytePayloadLength
     * @param jarName
     * @param c
     * @param datatype
     * @param paramPosition 
     * @throws IOException
     * @throws NoConnectionsException
     * @throws ProcCallException
     */
    private static void createTables(int bytePayloadLength, String jarName, Client c, String datatype, String classname)
            throws IOException, NoConnectionsException, ProcCallException {
        c.callProcedure("@AdHoc", "CREATE TABLE sort_test (pk_1 varchar(100) not null, pk_2 " + datatype
                + " not null, payload varbinary(" + bytePayloadLength + "), primary key (pk_1, pk_2));");

        c.callProcedure("@AdHoc", "PARTITION TABLE sort_test ON COLUMN pk_2;");

        c.callProcedure("@AdHoc",
                "CREATE PROCEDURE global_read AS SELECT * FROM sort_test WHERE pk_1 = ? AND pk_2 >= ? ORDER BY pk_2 LIMIT ?;");
        c.callProcedure("@UpdateClasses", getFile(jarName), "");
        c.callProcedure("@AdHoc",
                "CREATE PROCEDURE PARTITION ON TABLE sort_test COLUMN pk_2 FROM CLASS PartitionedRead"+classname+";");
    }

    /**
     * @param c
     * @throws IOException
     * @throws NoConnectionsException
     * @throws ProcCallException
     */
    private static void dropTables(Client c, String jarName) throws IOException, NoConnectionsException, ProcCallException {

        c.callProcedure("@AdHoc", "DROP PROCEDURE global_read IF EXISTS;");
        c.callProcedure("@AdHoc", "DROP PROCEDURE PartitionedReadVarchar IF EXISTS;");
        c.callProcedure("@AdHoc", "DROP PROCEDURE PartitionedReadBigint IF EXISTS;");
        c.callProcedure("@AdHoc", "DROP PROCEDURE PartitionedReadTS IF EXISTS;");
        c.callProcedure("@AdHoc", "DROP TABLE sort_test IF EXISTS;");
    }

    /**
     * @param c
     * @param datatypeClassname 
     * @param datatypes 
     * @throws IOException
     * @throws NoConnectionsException
     * @throws ProcCallException
     */
    private static void testValues(Client c, String pk1, Object object, int count, String datatypeClassname)
            throws IOException, NoConnectionsException, ProcCallException {
        msg("test " + datatypeClassname + " " + pk1 + " " + object + " " + count);
        ClientResponse crGlobal = c.callProcedure("global_read", pk1, object, count);

        Object objectParam = null;
   
        if (object instanceof String) {
            objectParam = (String) object;
        } else if (object instanceof Long) {
            objectParam = (Long) object;
        }

        ClientResponseWithPartitionKey[] crPartitionedArray = c.callAllPartitionProcedure("PartitionedRead"+datatypeClassname, pk1,
                objectParam, count);
        VoltDBTableSortedMergeWrangler w = new VoltDBTableSortedMergeWrangler(crPartitionedArray);

        VoltTable partitionResults = null;
        try {
            partitionResults = w.getSortedTable(1, count);
        } catch (Exception e) {
            msg(e.getMessage());
            System.exit(3);
        }

        String global = crGlobal.getResults()[0].toFormattedString();
        String partitioned = partitionResults.toFormattedString();

        
        assertEquals(pk1 + " " + object + " " + count + ": COUNT DIFFERS:"
                + crGlobal.getResults()[0].getRowCount() + " != " + partitionResults.getRowCount(), crGlobal.getResults()[0].getRowCount(), partitionResults.getRowCount());
        
        if (crGlobal.getResults()[0].getRowCount() != partitionResults.getRowCount()) {
            System.err.println(pk1 + " " + object + " " + count + ": COUNT DIFFERS:"
                    + crGlobal.getResults()[0].getRowCount() + " != " + partitionResults.getRowCount());
            System.err.println(global);
            System.err.println(partitioned);
            System.exit(1);
        }

        
        assertEquals(pk1 + " " + object + " " + count + ": CONTENT DIFFERS", global, partitioned); 
        
        if (!global.equals(partitioned)) {
            System.err.println(pk1 + " " + object + " " + count + ": CONTENT DIFFERS");
            System.err.println(global);
            System.err.println(partitioned);
            System.exit(2);
        }
    }

    private static byte[] makeBinaryPayload(Random r, int length) {

        byte[] payload = new byte[length];

        for (int i = 0; i < payload.length; i++) {
            payload[i] = (byte) (r.nextInt(255) - 127);
        }

        return payload;
    }

    private static Client connectVoltDB(String hostname) throws Exception {
        Client client = null;
        ClientConfig config = null;

        try {
            msg("Logging into VoltDB");

            config = new ClientConfig(); // "admin", "idontknow");
            config.setMaxOutstandingTxns(20000);
            config.setMaxTransactionsPerSecond(200000);
            config.setTopologyChangeAware(true);
            config.setReconnectOnConnectionLoss(true);

            client = ClientFactory.createClient(config);

            String[] hostnameArray = hostname.split(",");

            for (int i = 0; i < hostnameArray.length; i++) {
                msg("Connect to " + hostnameArray[i] + "...");
                try {
                    client.createConnection(hostnameArray[i]);
                } catch (Exception e) {
                    msg(e.getMessage());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("VoltDB connection failed.." + e.getMessage(), e);
        }

        return client;

    }

    public static void msg(String message) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate + ":" + message);
    }

    public static byte[] getFile(String filename) throws IOException {

        File file = new File(filename);
        // init array with file length
        byte[] bytesArray = new byte[(int) file.length()];

        FileInputStream fis = new FileInputStream(file);
        fis.read(bytesArray); // read file into bytes[]
        fis.close();

        return bytesArray;
    }

}

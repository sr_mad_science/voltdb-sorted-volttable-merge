


import org.voltdb.client.ClientResponse;
import org.voltdb.client.ProcedureCallback;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2019 VoltDB Inc.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with VoltDB.  If not, see <http://www.gnu.org/licenses/>.
 */

public class ComplainOnErrorCallback implements ProcedureCallback {
    

    @Override
    public void clientCallback(ClientResponse arg0) throws Exception {
        
        if (arg0.getStatus() != ClientResponse.SUCCESS) {
            System.err.println("Error Code " + arg0.getStatusString());
        }

        if (arg0.getAppStatusString() != null && arg0.getAppStatusString().length() != 0) {
            System.err.println("App Status: " + arg0.getAppStatusString());
        }
        
 
    }

}

# voltdb-sorted-volttable-merge

This repo contains VoltDBTableSortedMergeWrangler, which allows you to merge volttables provided by [callAllPartitionProcedure](https://docs.voltdb.com/javadoc/java-client-api/org/voltdb/client/Client.html#callAllPartitionProcedure-java.lang.String-java.lang.Object...-).

The intended use case is for when you need to issue a multi partition query but would prefer not to, as you don't need perfect read consistency and would rather get the individual VoltDB partitions to issue the query independently and then somehow merge the results.

A example of this scenario would be:

    CREATE TABLE sort_test (pk_1 varchar(100) not null, pk_2 varchar(100) not null, payload varbinary(100), primary key (pk_1, pk_2));
    PARTITION TABLE sort_test ON COLUMN pk_2;
                
If you need a sorted list of rows where pk_1 = 'x', starting with pk_2 = 'y' and sorted by pk_2 you can do this:                
                
    CREATE PROCEDURE global_read AS SELECT * FROM sort_test WHERE pk_1 = ? AND pk_2 >= ? ORDER BY pk_2 LIMIT ?;
    
But 'global_read' will use a multi partition read. If you need a perfectly read consistent view of the data then this is ideal, but if not you could
use callAllPartitionProcedure to break it down into a per-partition workload:

    public class PartitionedReadVarchar extends VoltProcedure {
        public final SQLStmt readStmt = new SQLStmt(
            "SELECT * FROM sort_test WHERE pk_1 = ? AND pk_2 >= ? ORDER BY pk_2 LIMIT ?");
        public VoltTable[] run(String partKey, String pk1, String pk2Start, int count) throws Exception {
            voltQueueSQL(readStmt, pk1, pk2Start, count);
            return voltExecuteSQL(true);
        }
    }

There are two problems with this:

1. 'callAllPartitionProcedure' returns an array of [ClientResponseWithPartitionKey](https://docs.voltdb.com/javadoc/java-client-api/index.html?org/voltdb/client/ClientResponseWithPartitionKey.html) instead of a single [VoltTable](https://docs.voltdb.com/javadoc/java-client-api/org/voltdb/VoltTable.html). 

2. If you wanted to LIMIT the results you can pass in your LIMIT value to the procedure, but then you will get up to LIMIT rows for each partition.

'VoltDBTableSortedMergeWrangler' takes [ClientResponseWithPartitionKey](https://docs.voltdb.com/javadoc/java-client-api/index.html?org/voltdb/client/ClientResponseWithPartitionKey.html) and creates a new VoltTable by merging the first VoltTable in each [ClientResponseWithPartitionKey](https://docs.voltdb.com/javadoc/java-client-api/index.html?org/voltdb/client/ClientResponseWithPartitionKey.html) . It assumes that all the VoltTables are sorted corrrectly. It can be told how many rows its to limit the output VoltTable size to. Currently it supports VARCHAR and BIGINT. 
